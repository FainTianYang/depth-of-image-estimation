# -*- coding: utf-8 -*- 
#
# GoogLeNet Image Classification

from keras.layers.core import Layer
from keras.models import Model
from keras.layers import Input, Dense, Convolution2D, MaxPooling2D, AveragePooling2D, ZeroPadding2D, Dropout, Flatten, merge, Reshape, Activation
from keras.regularizers import l2
from keras.optimizers import RMSprop
from keras.optimizers import SGD
from keras import backend as K

from keras.preprocessing.image import ImageDataGenerator
from keras.utils.data_utils import get_file

import numpy as np
import random
import sys, os
import ConfigParser, argparse
import pymysql
from FetchImageDataset import *

def CNN_VGG16(weights_path=None, labels=1000) :
    input = Input(shape=(224, 224, 3))

    model = Sequential()
    model.add(ZeroPadding2D((1,1),input))
    model.add(Convolution2D(64, 3, 3, activation='relu'))
    model.add(ZeroPadding2D((1,1)))
    model.add(Convolution2D(64, 3, 3, activation='relu'))
    model.add(MaxPooling2D((2,2), strides=(2,2)))

    model.add(ZeroPadding2D((1,1)))
    model.add(Convolution2D(128, 3, 3, activation='relu'))
    model.add(ZeroPadding2D((1,1)))
    model.add(Convolution2D(128, 3, 3, activation='relu'))
    model.add(MaxPooling2D((2,2), strides=(2,2)))

    model.add(ZeroPadding2D((1,1)))
    model.add(Convolution2D(256, 3, 3, activation='relu'))
    model.add(ZeroPadding2D((1,1)))
    model.add(Convolution2D(256, 3, 3, activation='relu'))
    model.add(ZeroPadding2D((1,1)))
    model.add(Convolution2D(256, 3, 3, activation='relu'))
    model.add(MaxPooling2D((2,2), strides=(2,2)))

    model.add(ZeroPadding2D((1,1)))
    model.add(Convolution2D(512, 3, 3, activation='relu'))
    model.add(ZeroPadding2D((1,1)))
    model.add(Convolution2D(512, 3, 3, activation='relu'))
    model.add(ZeroPadding2D((1,1)))
    model.add(Convolution2D(512, 3, 3, activation='relu'))
    model.add(MaxPooling2D((2,2), strides=(2,2)))

    model.add(ZeroPadding2D((1,1)))
    model.add(Convolution2D(512, 3, 3, activation='relu'))
    model.add(ZeroPadding2D((1,1)))
    model.add(Convolution2D(512, 3, 3, activation='relu'))
    model.add(ZeroPadding2D((1,1)))
    model.add(Convolution2D(512, 3, 3, activation='relu'))
    model.add(MaxPooling2D((2,2), strides=(2,2)))

    model.add(Flatten())
    model.add(Dense(4096, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(4096, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(labels, activation='softmax'))

    if weights_path:
        model.load_weights(weights_path)

    return model


if __name__ == '__main__':

    dataDir = 'data'

    # Create googlenet model and plot it
    from keras.utils.visualize_util import plot
    model = CNN_VGG16(labels=3)
    plot(model, to_file='cnn_googlenet.png')

    model.compile(loss='categorical_crossentropy', optimizer=RMSprop(lr=0.01))

    # Preparing image dataset
    g_dbconfig = {
            'host' : 'localhost',
            'user' : 'root',
            'passwd' : 'cggm54787',
            'db' : 'depth_learning',
            'charset' : 'utf8'
    }

    print("Connecting database...")
    db = pymysql.connect(**g_dbconfig)
    cursor = db.cursor(pymysql.cursors.DictCursor)
    cursor.execute("SET NAMES utf8")
    cursor.execute("SET CHARACTER_SET_CLIENT=utf8")
    cursor.execute("SET CHARACTER_SET_RESULTS=utf8")
    db.commit()

    # get event list
    print("Fetching training images...")
    cursor.execute("select * from ILSVRC2012_3class")
    imageList = cursor.fetchall()

    # Fetch image files from url to local directory
#    imageList = util_FetchImageDataset(imageList, dataDir)

    # Create data input
    datagen = ImageDataGenerator(
            featurewise_center=True,
            featurewise_std_normalization=True,
            rotation_range=20,
            width_shift_range=0.2,
            height_shift_range=0.2,
            horizontal_flip=True
        )

    train_generator = datagen.flow_from_directory(
            dataDir,
            target_size=(224, 224),
            batch_size=32,
            class_mode='categorical'
        )

    val_generator = datagen.flow_from_directory(
            dataDir,
            target_size=(224, 224),
            batch_size=32,
            class_mode='categorical'
        )

    # model.fit(X, X, batch_size=128, nb_epoch=1)

#    model.fit_generator(
#            train_generator,
#            samples_per_epoch=8,
#            nb_epoch=10,
#            validation_data=val_generator,
#            nb_val_samples=8
#        )

    nb_epoch = 10
    for e in range(nb_epoch):
        batches = 0
        for X_batch, Y_batch in train_generator:
            loss = model.train_on_batch(X_batch, [Y_batch,Y_batch,Y_batch]) # note the three outputs
            print('\tepoch/batch '+str(e)+'/'+str(batches)+' : train loss='+str(loss))
            batches += 1
            if batches >= train_generator.nb_sample / train_generator.batch_size:
                # we need to break the loop by hand because
                # the generator loops indefinitely
                break
