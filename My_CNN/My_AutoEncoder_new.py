import numpy as np
import time
np.random.seed(1337)  # for reproducibility

import tensorflow as tf
from keras.preprocessing.image import ImageDataGenerator
from keras.datasets import mnist
from keras.models import Model
from keras.layers import Dense, Input, Convolution2D, MaxPooling2D, UpSampling2D
from keras.utils.data_utils import get_file
from keras.utils.visualize_util import plot
import matplotlib
import matplotlib.pyplot as plt
matplotlib.use('Agg')

def AutoEncoder_model(Weight_path=None, image_shape=(128, 128, 3)):
	input_img = Input(image_shape)	

	# encoder layers
	encoded = Convolution2D(16, 3, 3, activation='relu', border_mode='same')(input_img)
	#output = (16,128,128)
	encoded = MaxPooling2D((2, 2), border_mode='same')(encoded)
	#output = (16,64,64)
	encoded = Convolution2D(8, 3, 3, activation='relu', border_mode='same')(encoded)
	#output = (8,64,64)
	encoded = MaxPooling2D((2, 2), border_mode='same')(encoded)
	#output = (8,32,32)
	encoded = Convolution2D(8, 3, 3, activation='relu', border_mode='same')(encoded)
	#output = (8,32,32)
	encoder_output = MaxPooling2D((2, 2), border_mode='same')(encoded)
	#output = (8,16,16)

	# decoder layers
	decoded = Convolution2D(8, 3, 3, activation='relu', border_mode='same')(encoder_output)
	#output = (8,16,16)
	decoded = UpSampling2D((2, 2))(decoded)
	#output = (8,32,32)
	decoded = Convolution2D(8, 3, 3, activation='relu', border_mode='same')(decoded)
	#output = (8,32,32)
	decoded = UpSampling2D((2, 2))(decoded)
	#output = (8,64,64)
	decoded = Convolution2D(16, 3, 3, activation='relu', border_mode='same')(decoded) #Because value is -0.5~0.5 before encoded, the tanh is -1~1 can has a good maping
	#output = (16,64,64)
	decoded = UpSampling2D((2, 2))(decoded)
	#output = (16,128,128)
	decoded = Convolution2D(3, 3, 3, activation='sigmoid', border_mode='same')(decoded)
	#output = (3,128,128)
	
	# construct the autoencoder model
	autoencoder = Model(input=input_img, output=decoded)
	
	plot(autoencoder, show_shapes=True, to_file='My_AutoEncoder_new.png')
	
	if Weight_path:
		autoencoder.load_weights(Weight_path)

	return autoencoder
	
if __name__ == '__main__':

	My_batch_size = 64
	data_path = '/home/DepthLearning/cggm1/Image' #image=(1080,1920)
	image_w = 128
	image_h = 128
	Weight_path = None
	
	print("Process image......")
	train_datagen = ImageDataGenerator(
				rescale=1./255,
				featurewise_center=True,
				featurewise_std_normalization=True,
				rotation_range=10,
				width_shift_range=0.05,
				height_shift_range=0.05,
				horizontal_flip=True)
					
	test_datagen = ImageDataGenerator(rescale=1./255)

	train_generator = train_datagen.flow_from_directory(
				data_path,
				target_size=(image_w, image_h),
				batch_size=My_batch_size)

	test_generator = test_datagen.flow_from_directory(
				data_path,
				target_size=(image_w, image_h),
				batch_size=My_batch_size)

	print("Process image finish......")
	
	print("Build model......")
	
	model = AutoEncoder_model(Weight_path, image_shape=(image_w, image_h, 3))
	
	model.compile(optimizer='adam', loss='mae')
	
	print("Build model finish......")
	
	if (Weight_path==None):
		print("Start training......")
		My_nb_epoch = 10
		for e in range(My_nb_epoch):
			batches = 0
			for X_batch, Y_batch in train_generator:
				loss = model.train_on_batch(X_batch, X_batch) # note the three outputs
				print('\tepoch/batch '+str(e)+'/'+str(batches)+' : train loss='+str(loss))
				batches += 1
				if batches >= train_generator.nb_sample / train_generator.batch_size: 
					break
		
		print("Finish training......")		
		model.save_weights("My_AutoEncoder_new_10_64.h5")
	
#Predict & save image---------------------------------------------------------------------------------------------------
    
	print('Predict & Save image..........')
	X_batch_num = 0
	for X_batch, Y_batch in train_generator :
		decoded_imgs = model.predict(X_batch)
		height = float(image_h)
		width = float(image_w)
		if X_batch_num < 1 :
			for i in range(10):
				# display original
				or_fig = plt.figure()
				or_fig.set_size_inches(width/height, 1, forward=False)
				ax = plt.Axes(or_fig, [0., 0., 1., 1.])
				ax.set_axis_off()
				or_fig.add_axes(ax)
 
				ax.imshow(X_batch[i].reshape(image_w, image_h,3), aspect='normal')
				image_name = 'or_' + str(X_batch_num) + '_' + str(i) + '.png'
				file_name = './image_10_64/'
				plt.savefig(file_name + image_name, dpi = height)
				plt.close()
				
				# display reconstruction
				af_fig = plt.figure()
				af_fig.set_size_inches(width/height, 1, forward=False)
				ax = plt.Axes(af_fig, [0., 0., 1., 1.])
				ax.set_axis_off()
				af_fig.add_axes(ax)
				
				ax.imshow(decoded_imgs[i].reshape(image_w, image_h,3), aspect='normal')
				image_name = 'af_' + str(X_batch_num) + '_' + str(i) + '.png'
				file_name = './image_10_64/'
				plt.savefig(file_name + image_name, dpi = height)
				plt.close()
		else :
			break
		X_batch_num+=1