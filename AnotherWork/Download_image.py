'''
Download image from URL of tables
'''
import pymysql
import shutil 
import requests 

# connect to database
dbconfig = {
    'host' : 'localhost',
    'user' : 'root',
    'passwd' : 'cggm54787',
    'charset' : 'utf8',
    'db' : 'depth_learning'
}
db = pymysql.connect(**dbconfig)
cur = db.cursor(pymysql.cursors.DictCursor)
cur.execute("select * from ILSVRC2012_100_traning")
num_rows = int(cur.rowcount)
print str(num_rows)
count = 1

for i in range(0,num_rows,1):
  row = cur.fetchone()
  image_name = str((row["ID"]))+".jpg"
  image_url = str(row["URL"])
  #print image_name +" , "+ image_url
  
  try:
    response = requests.get(image_url , stream = True , timeout = 3)
  
    if response.status_code == requests.codes.ok:
      with open('/home/DepthLearning/ILSVRC12_100/'+image_name,'wb') as out_file:
        shutil.copyfileobj(response.raw, out_file) 
      del response
    else:
      print image_name
      count = count+1
  except requests.exceptions.ConnectTimeout as err:
      print image_name
      count = count+1
  except requests.exceptions.HTTPError as err:
      print image_name
      count = count+1
  except requests.exceptions.ConnectionError as err:
      print image_name
      count = count+1

print str(count)
