import numpy as np
import time
import random
import sys, os
import ConfigParser, argparse
np.random.seed(1337)  # for reproducibility

import tensorflow as tf
from keras.preprocessing.image import ImageDataGenerator
from keras.datasets import mnist
from keras.layers.core import Layer
from keras.models import Model, Sequential
from keras.layers import Dense, Input, Convolution2D, MaxPooling2D, UpSampling2D, AveragePooling2D, ZeroPadding2D, Dropout, Flatten, merge, Reshape, Activation
from keras.regularizers import l2
from keras.optimizers import RMSprop
from keras.optimizers import SGD
from keras import backend as K

from keras.utils.data_utils import get_file
from keras.utils.visualize_util import plot

import matplotlib
import matplotlib.pyplot as plt
matplotlib.use('Agg')

def Model_AutoEncoder_VGG(weights_path=None, image_shape=(256, 256, 3)):

	model = Sequential()
	model.add(Convolution2D(64, 3, 3, border_mode='same', input_shape=image_shape, activation='relu'))
	model.add(Convolution2D(64, 3, 3, activation='relu', border_mode='same'))
	model.add(MaxPooling2D((2,2), border_mode='same'))
	#output(64,128,128)
	
	model.add(Convolution2D(128, 3, 3, activation='relu', border_mode='same'))
	model.add(Convolution2D(128, 3, 3, activation='relu', border_mode='same'))
	model.add(MaxPooling2D((2,2), border_mode='same'))
	#output(128,64,64)
	
	model.add(Convolution2D(256, 3, 3, activation='relu', border_mode='same'))
	model.add(Convolution2D(256, 3, 3, activation='relu', border_mode='same'))
	model.add(Convolution2D(256, 3, 3, activation='relu', border_mode='same'))
	model.add(MaxPooling2D((2,2), border_mode='same'))
	#output(256,32,32)
	
	model.add(Convolution2D(512, 3, 3, activation='relu', border_mode='same'))
	model.add(Convolution2D(512, 3, 3, activation='relu', border_mode='same'))
	model.add(Convolution2D(512, 3, 3, activation='relu', border_mode='same'))
	model.add(MaxPooling2D((2,2), border_mode='same'))
	#output(512,16,16)

	model.add(Convolution2D(512, 3, 3, activation='relu', border_mode='same'))
	model.add(Convolution2D(512, 3, 3, activation='relu', border_mode='same'))
	model.add(Convolution2D(512, 3, 3, activation='relu', border_mode='same'))
	model.add(MaxPooling2D((2,2), border_mode='same'))
	#output(512,8,8)
	
	model.add(Flatten())
	#output(32768)
	model.add(Dense(4096, activation='relu'))
	model.add(Dropout(0.5))
	model.add(Dense(4096, activation='relu'))
	model.add(Dropout(0.5))
	
	model.add(Dense(32768, activation='relu'))
	model.add(Dropout(0.5))
	model.add(Dense(32768, activation='relu'))
	model.add(Dropout(0.5))
	model.add(Reshape((8,8,512)))
	#output(512,8,8)
	
	model.add(UpSampling2D((2, 2)))
	model.add(Convolution2D(512, 3, 3, activation='relu', border_mode='same'))
	model.add(Convolution2D(512, 3, 3, activation='relu', border_mode='same'))
	model.add(Convolution2D(512, 3, 3, activation='relu', border_mode='same'))
	#output(512,16,16)
	
	model.add(UpSampling2D((2, 2)))
	model.add(Convolution2D(512, 3, 3, activation='relu', border_mode='same'))
	model.add(Convolution2D(512, 3, 3, activation='relu', border_mode='same'))
	model.add(Convolution2D(512, 3, 3, activation='relu', border_mode='same'))
	#output(512,32,32)
	
	model.add(UpSampling2D((2, 2)))
	model.add(Convolution2D(256, 3, 3, activation='relu', border_mode='same'))
	model.add(Convolution2D(256, 3, 3, activation='relu', border_mode='same'))
	model.add(Convolution2D(256, 3, 3, activation='relu', border_mode='same'))
	#output(256,64,64)
	
	model.add(UpSampling2D((2, 2)))
	model.add(Convolution2D(128, 3, 3, activation='relu', border_mode='same'))
	model.add(Convolution2D(128, 3, 3, activation='relu', border_mode='same'))
	#output(128,128,128)
	
	model.add(UpSampling2D((2, 2)))
	model.add(Convolution2D(64, 3, 3, activation='relu', border_mode='same'))
	model.add(Convolution2D(64, 3, 3, activation='relu', border_mode='same'))
	#output(64,256,256)
	
	model.add(Convolution2D(3, 3, 3, activation='sigmoid', border_mode='same'))
	#output(3,256,256)
	
	plot(model, show_shapes=True, to_file='Auto_VGG.png')
	
	if weights_path:
		model.load_weights(weights_path)

	return model
	
if __name__ == '__main__':

	My_batch_size = 512
	data_path = '/home/DepthLearning/cggm1/Image' #image=(1080,1920)
	image_w = 256
	image_h = 256

	print("Process image......")
	train_datagen = ImageDataGenerator(
				rescale=1./255,
				featurewise_center=True,
				featurewise_std_normalization=True,
				rotation_range=10,
				width_shift_range=0.05,
				height_shift_range=0.05,
				horizontal_flip=True
			)
		
	test_datagen = ImageDataGenerator(rescale=1./255)

	train_generator = train_datagen.flow_from_directory(
				data_path,
				target_size=(image_w, image_h),
				batch_size=My_batch_size
			)

	test_generator = test_datagen.flow_from_directory(
				data_path,
				target_size=(image_w, image_h),
				batch_size=My_batch_size
			)

	print("Process image finish......")
	
	print("Build model......")
	model = Model_AutoEncoder_VGG(image_shape=(image_w, image_h, 3))
	model.compile(optimizer='adam', loss='mae')
	print("Build model finish......")
	
	print("Start training......")
	My_nb_epoch = 5
	for e in range(My_nb_epoch):
		batches = 0
		for X_batch, Y_batch in train_generator:
			loss = model.train_on_batch(X_batch, X_batch) # note the three outputs
			print('\tepoch/batch '+str(e)+'/'+str(batches)+' : train loss='+str(loss))
			batches += 1
			if batches >= train_generator.nb_sample / train_generator.batch_size: 
				break
		
	print("Finish training......")		
	model.save_weights("Auto_VGG.h5")