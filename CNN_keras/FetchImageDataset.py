# -*- coding: utf-8 -*- 
#
# Utility to fetch image files from url table.
# A very simple CNN example with image dataset as input.
#

import sys, os
import urllib3
from PIL import Image


def util_FetchImageDataset(urlList, dataPath, refresh=False, chunk_size=1024) :
    # create data folder
    if not os.path.exists(dataPath) :
        os.mkdir(dataPath)

    retrievedList = []

    http = urllib3.PoolManager()
    # fetch all images
    iCounter = 1
    for img in urlList :
        filename = os.path.join(dataPath, img['Label'], os.path.basename(img['URL']))
        filename = filename.split('?')[0]
        # Create directory (label) if not exists
        if not os.path.exists(os.path.dirname(filename)) :
            os.mkdir(os.path.dirname(filename))
        # no refresh data and file exists
        if not refresh and os.path.exists(filename) :
            print("\t"+str(iCounter)+"/"+str(len(urlList))+" : Retriving file '"+os.path.basename(img['URL'])+"' locally existed")
            img['local_file'] = filename
            retrievedList.append(img)
        else :
            try :
                imgFile = http.request('GET', img['URL'], preload_content=False)
                data = imgFile.read(chunk_size)
                if data :
                    print("\t"+str(iCounter)+"/"+str(len(urlList))+" : Retriving file '"+os.path.basename(img['URL'])+"' ...")
                    with open(filename, 'wb') as f:
                        while True:
                            if not data:
                                break
                            f.write(data)
                            data = imgFile.read(chunk_size)
                        imgFile.release_conn()
                        f.close()
                        # test if image file is valid
                        try :
                            im = Image.open(filename)
                            img['local_file'] = filename
                            retrievedList.append(img)
                        except :
                            print("\t\tError : Not a valid image file.")
                            os.remove(filename)
                            pass
                else :
                    print("\t"+str(iCounter)+"/"+str(len(urlList))+" : Retriving file '"+os.path.basename(img['URL'])+"' failed")
            except :
                print("\t"+str(iCounter)+"/"+str(len(urlList))+" : Error on retriving URL '"+img['URL']+"', no such url?")
                pass
        iCounter += 1
    return retrievedList


if __name__ == '__main__':
    from keras.models import Sequential
    from keras.layers import Dense, Activation, Flatten
    from keras.layers import Convolution2D, MaxPooling2D
    from keras.optimizers import RMSprop
    from keras.preprocessing.image import ImageDataGenerator
    from keras.utils.data_utils import get_file
    import pymysql

    dataDir = 'data'

    g_dbconfig = {
            'host' : 'localhost',
            'user' : 'root',
            'passwd' : 'cggm54787',
            'db' : 'depth_learning',
            'charset' : 'utf8'
    }

    print("Connecting database...")
    db = pymysql.connect(**g_dbconfig)
    cursor = db.cursor(pymysql.cursors.DictCursor)
    cursor.execute("SET NAMES utf8")
    cursor.execute("SET CHARACTER_SET_CLIENT=utf8")
    cursor.execute("SET CHARACTER_SET_RESULTS=utf8")
    db.commit()

    # get event list
    print("Fetching training images...")
    cursor.execute("select * from ILSVRC2012_3class")
    imageList = cursor.fetchall()

    # Fetch image files from url to local directory
    imageList = util_FetchImageDataset(imageList, dataDir)

    datagen = ImageDataGenerator(
            featurewise_center=True,
            featurewise_std_normalization=True,
            rotation_range=20,
            width_shift_range=0.2,
            height_shift_range=0.2,
            horizontal_flip=True
        )

    train_generator = datagen.flow_from_directory(
            dataDir,
            target_size=(256, 256),
            batch_size=32,
            class_mode='categorical'
        )

    val_generator = datagen.flow_from_directory(
            dataDir,
            target_size=(256, 256),
            batch_size=32,
            class_mode='categorical'
        )


    # build the model
    print('Build model...')
    model = Sequential()
    model.add(Convolution2D(train_generator.nb_class, 3, 3, border_mode='same', input_shape=(256, 256, 3)))
    # output shape (None, 256, 256, 64)
    model.add(Activation('relu'))
    model.add(MaxPooling2D((2, 2), input_shape=(256, 256, 3)))
    # output shape (None, 128, 128, 64)
    #model.add(Convolution2D(64, 3, 3, border_mode='same'))
    model.add(MaxPooling2D((2, 2)))
    # output shape (None, 64, 64, 64)
    #model.add(Convolution2D(64, 3, 3, border_mode='same'))
    model.add(MaxPooling2D((2, 2)))
    # output shape (None, 32, 32, 64)
    #model.add(Convolution2D(64, 3, 3, border_mode='same'))
    model.add(MaxPooling2D((2, 2)))
    # output shape (None, 16, 16, 64)
    #model.add(Convolution2D(64, 3, 3, border_mode='same'))
    model.add(MaxPooling2D((2, 2)))
    # output shape (None, 8, 8, 64)
    #model.add(Convolution2D(64, 3, 3, border_mode='same'))
    model.add(MaxPooling2D((2, 2)))
    # output shape (None, 4, 4, 64)
    #model.add(Convolution2D(64, 3, 3, border_mode='same'))
    model.add(MaxPooling2D((2, 2)))
    # output shape (None, 2, 2, 64)
    #model.add(Convolution2D(64, 3, 3, border_mode='same'))
    model.add(MaxPooling2D((2, 2)))
    # output shape (None, 1, 1, 64)
    model.add(Flatten())
    # output shape (None, 1*1*64)

    model.add(Activation('softmax'))

    model.compile(loss='categorical_crossentropy', optimizer=RMSprop(lr=0.01))

    from keras.utils.visualize_util import plot
    plot(model, to_file='cnn_model.png')

    # train the model, output generated text after each iteration
    print()
    print('-' * 50)
    # model.fit(X, X, batch_size=128, nb_epoch=1)
    model.fit_generator(
            train_generator,
            samples_per_epoch=8,
            nb_epoch=10,
            validation_data=val_generator,
            nb_val_samples=8
        )
